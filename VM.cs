﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace FileDemo
{
    
    class VM : INotifyPropertyChanged
    {
        string data;

        public string TheContent
        {
            get { return data; }
            set { data = value; onChange(); }
        }

        public void ShowData()
        {
            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader("output.txt"))
                {
                    // Read the stream to a string, and write the string to the console.
                    TheContent = sr.ReadToEnd();
                    Console.WriteLine(TheContent);
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
        }

        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        private void onChange([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
