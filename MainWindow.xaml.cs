﻿using System.Windows;

namespace FileDemo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        VM vm = new VM();
        public MainWindow()
        {
            InitializeComponent();
            DataContext = vm;
        }

        private void btnshow_Click(object sender, RoutedEventArgs e)
        {
            vm.ShowData();
        }
    }
}
